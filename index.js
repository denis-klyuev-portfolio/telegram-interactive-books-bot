const token = require('./package').telegram.token;
const App = require('./src/App');
const Update = require('./src/Telegram/Update');

exports.handler = async event => {
    const stage = event.stageVariables.lambdaVersion;
    // console.log(`path comparison for ${stage}`, event.path, `/${token[stage]}`);
    if (!event.body || event.path !== `/${token[stage]}`) {
        return {
            'statusCode': 403,
            'body': '403 Forbidden'
        };
    }

    const body = event.isBase64Encoded
        ? Buffer.from(event.body, 'base64').toString('utf-8')
        : event.body;
    // console.log('event', JSON.stringify(event));
    // console.log('post body', body);
    const jsonBody = JSON.parse(body);
    // console.log('parsed body', jsonBody);
    const appResponse = await App.reflectTo(Update.parse(jsonBody));
    // console.log('app response', appResponse);
    return {
        'statusCode': 200,
        'headers': {
            'content-type': 'application/json'
        },
        'body': appResponse ? JSON.stringify(appResponse) : ''
    };
};
