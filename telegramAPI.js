const https = require('https');
const querystring = require('querystring');
const argv = require('yargs').argv;

const claudiaConfig = require('./claudia.json');
const token = require('./package').telegram.token;

const version = argv.ver || 'prod';

class TelegramAPI {
    static methodURL(method, endpoint) {
        return `https://api.telegram.org/bot${token[endpoint]}/${method}`;
    }

    static request(method, params, endpoint) {
        return new Promise((resolve, reject) => {
            const [_, protocol, hostname, path] = TelegramAPI.methodURL(method, endpoint).match(/^(https?):\/\/([^/]+)(.+)$/);
            // console.log(protocol, hostname, path);
            const postData = querystring.stringify(params);

            const options = {
                hostname,
                port: 443,
                path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };

            console.log(postData);

            let responseData = '';
            const req = https.request(options, (res) => {
                // console.log(`STATUS: ${res.statusCode}`);
                // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', chunk => {
                    responseData += chunk;
                });
                res.on('end', () => resolve(responseData));
            });

            req.on('error', e => reject(e));

            req.write(postData);
            req.end();
        });
    }
}
const setWebhook = endpoint => TelegramAPI.request('setWebhook', {
    url: `https://${claudiaConfig.api.id}.execute-api.${claudiaConfig.lambda.region}.amazonaws.com/${endpoint}/${token[endpoint]}`,
    max_connections: 100,
    allowed_updates: [
        'message',
        'edited_message',
        'edited_channel_post',
        'channel_post',
        'inline_query',
        'chosen_inline_result',
        'callback_query',
    ]
}, endpoint);
const getWebhookInfo = endpoint => TelegramAPI.request('getWebhookInfo', {}, endpoint);

setWebhook(version)
    .then(data => console.log('success!', data))
    .catch(e => console.log('error!', e));
getWebhookInfo(version)
    .then(data => console.log('success!', data))
    .catch(e => console.log('error!', e));
