import path from 'path';
const autoprefixer = require('autoprefixer');

export default function (options = {}) {
    const config = {
        entry: path.resolve(__dirname, './dev-server/src/index'),

        output: {
            path: path.resolve(__dirname, './dev-server/public'),
            publicPath: '',
            filename: 'bundle.js',
        },

        devtool: 'cheap-module-source-map',
        devServer: {
            compress: false,
            open: true,
            openPage: '',
            watchContentBase: false,
            contentBase: [
                path.resolve(__dirname, './dev-server/public')
            ],
            stats: {
                assets: false,
                colors: true,
                version: true,
                hash: false,
                timings: true,
                chunks: false,
                chunkModules: false,
                chunkOrigins: false,
                children: false,
                modules: false,
                moduleTrace: false,
                reasons: false,
                source: false,
                entrypoints: false,
                cached: false,
                cachedAssets: false
            },
        },

        resolve: {
            extensions: ['.js'],
            modules: ['static', 'src', 'node_modules'],
            alias: {}
        },

        module: {
            rules: [{
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, './dev-server/src'),
                ],
                use: {
                    loader: 'babel-loader'
                },
            }, {
                test: /\.css$/,
                use: [
                    require.resolve('style-loader'),
                    {
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                        },
                    },
                    {
                        loader: require.resolve('postcss-loader'),
                        options: {
                            // Necessary for external CSS imports to work
                            // https://github.com/facebookincubator/create-react-app/issues/2677
                            ident: 'postcss',
                            plugins: () => [
                                require('postcss-flexbugs-fixes'),
                                autoprefixer({
                                    browsers: [
                                        '>1%',
                                        'last 4 versions',
                                        'Firefox ESR',
                                        'not ie < 9', // React doesn't support IE8 anyway
                                    ],
                                    flexbox: 'no-2009',
                                }),
                            ],
                        },
                    },
                ],
            },]
        },

        performance: {
            hints: 'warning'
        },
    };

    return config;
}
