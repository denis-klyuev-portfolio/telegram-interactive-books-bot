import React, {Component, Fragment} from 'react';
import ReactDom from 'react-dom';
import classnames from 'classnames';
import {MarkdownPreview} from 'react-marked-markdown';

import Api from './Api';

const Message = ({data}) => (
    <div className={classnames('message', {bot: data.bot})}>
        <MarkdownPreview value={data.text}/>
    </div>
);

class MessageList extends Component {
    scrollToBottom() {
        ReactDom.findDOMNode(this)
            .children[0]
            .scrollIntoView({
                behavior: 'smooth',
                block: 'end',
            });
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        return (
            <div className="messages">
                <div>
                    {this.props.list.map((item, index) => <Message data={item} key={index}/>)}
                </div>
            </div>
        );
    }
}
class TextBox extends Component {
    constructor(props) {
        super(props);

        // const {locked, onChange} = props;
        this.state = {
            text: '',
        };
    }

    handleTextChange = ({target: {value}}) => {
        if (this.props.locked) {
            return;
        }
        this.setState({
            text: value
        });
    };

    onEnter = () => {
        this.props.onChange(this.state.text);
    };

    render() {
        return (
            <div className={classnames('textbox', {locked: this.props.locked})}>
                <input type="text" placeholder="💬 Type your message here..." value={this.state.text} onChange={this.handleTextChange}/>
                <input type="button" onClick={this.onEnter} value="Send"/>
            </div>
        );
    }
}
class Toolbar extends Component {
    render() {
        return (
            <div className="toolbar">
                <h1>Telegram like debug app</h1>
                <div className="button" onClick={this.props.onClearMessages}>clear</div>
            </div>
        );
    }
}

const ReplyMenu = ({menu, onClick}) => (
    <div className={classnames('reply-menu', {visible: menu.length > 0})}>
        {menu.map((row, rowIndex) => <div key={rowIndex}>
            {row.map((button, buttonIndex) =>
                <input type="button" value={button.text} key={buttonIndex} onClick={() => onClick(button.command)}/>
            )}
        </div>)}
    </div>
);

class App extends Component {
    constructor(props) {
        super(props);
        let oldMessages = [];
        let oldReplyMenu = [[{text: 'Начать', command: '/start'}]];
        try {
            oldMessages = JSON.parse(localStorage.messagesCache);
            oldReplyMenu = JSON.parse(localStorage.replyMenuCache);
        }
        catch (e) {}
        this.state = {
            userId: 123,
            replyMenu: oldReplyMenu,
            messages: oldMessages,
            textBoxVisible: oldMessages.length > 0,
            textBoxLocked: false,
        };
    }

    handleTextSend = value => {
        const text = value.trim();
        const newMessages = [...this.state.messages, {
            bot: false,
            text,
        }];
        localStorage.messagesCache = JSON.stringify(newMessages);
        this.setState({
            textBoxLocked: true,
            textBoxVisible: true,
            messages: newMessages
        });
        Api.sendMessage(this.state.userId, text)
            .then(response => {
                this.parseResponse(response);
                this.setState({textBoxLocked: false});
            });
    };

    handleReplyMenuClick = value => {
        this.setState({
            replyMenu: []
        });
        this.handleTextSend(value);
    };


    parseResponse(response) {
        console.log(response);

        if (!response || response.method !== 'sendMessage') {
            return;
        }

        if (response.text) {
            const newMessages = [...this.state.messages, {
                bot: true,
                command: response.text,
                text: response.text,
            }];
            localStorage.messagesCache = JSON.stringify(newMessages);
            this.setState({
                messages: newMessages
            });
        }

        if (response.reply_markup) {
            const newReplyMenu = response.reply_markup.keyboard
                .map(row => row.map(({text}) => ({text, command: text})));
            localStorage.replyMenuCache = JSON.stringify(newReplyMenu);
            this.setState({
                replyMenu: newReplyMenu
            });
        }
    }

    handleClearMessages = () => {
        const newMessages = [];
        const newReplyMenu = [[{text: 'Начать', command: '/start'}]];
        localStorage.messagesCache = JSON.stringify(newMessages);
        localStorage.replyMenuCache = JSON.stringify(newReplyMenu);
        this.setState({
            messages: newMessages,
            replyMenu: newReplyMenu,
            textBoxVisible: false,
        });
    };

    render() {
        return (
            <div id="root-wrapper">
                <Toolbar onClearMessages={this.handleClearMessages}/>
                <MessageList list={this.state.messages}/>
                {this.state.textBoxVisible && <TextBox onChange={this.handleTextSend} locked={this.state.textBoxLocked} />}
                <ReplyMenu menu={this.state.replyMenu} onClick={this.handleReplyMenuClick}/>
            </div>
        );
    }
}

export default App;
