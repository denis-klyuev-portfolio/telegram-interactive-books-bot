export default class Api {
    static URL = '/api';
    static updateId = 1;
    static messageId = 1;

    static sendMessage(userId, text) {
        const exp = /(\/\w+)/;
        const entities = [];
        if (exp.test(text)) {
            const found = exp.exec(text);
            entities.push({
                offset: found.index,
                length: found[1].length,
                type: 'bot_command'
            });
        }
        const body = {
            "update_id": this.updateId++,
            "message": {
                "message_id": this.messageId++,
                "from": {
                    "id": userId,
                    "is_bot": false,
                    "first_name": "Name",
                    "last_name": "Surname",
                    "username": "user_name",
                    "language_code": "ru"
                },
                "chat": {
                    "id": userId,
                    "first_name": "Name",
                    "last_name": "Surname",
                    "username": "user_name",
                    "type": "private"
                },
                "date": Math.floor(Date.now() / 1000),
                "text": text,
                "entities": entities,
            }
        };

        return fetch(this.URL, {method: 'POST', headers: {
                'Content-Type': 'application/json'
            }, body: JSON.stringify(body)})
            .then(response => response.json());
    }
}
