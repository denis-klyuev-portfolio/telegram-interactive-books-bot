import path from 'path';
import express from 'express';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';
import open from 'open';
import http from 'http';

import config from './webpack.config.babel';
import {handler} from './index';

const app = express();
const bodyParser = require('body-parser');
const envConfig = config({dev: 'server'});
const compiler = webpack(envConfig);
const detect = require('detect-port');
const token = require('./package').telegram.token;

app.use(bodyParser.text({type: '*/*'}));

app.use('/api', async (req, res) => {
    const event = {
        stageVariables: {
            lambdaVersion: 'debug'
        },
        isBase64Encoded: false,
        body: req.body,
        path: `/${token.debug}`
    };
    const result = await handler(event);
    res.statusCode = result.statusCode;
    res.set(result.headers);
    res.send(result.body);
});

app.use(webpackDevMiddleware(compiler, {
    publicPath: envConfig.output.publicPath, // Same as `output.publicPath` in most cases.
}));

app.use(webpackHotMiddleware(compiler));
app.use(express.static(path.resolve(__dirname, './dev-server/public')));

const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000;
detect(DEFAULT_PORT).then(port => {
    const server = http.createServer(app).listen(port, () => {
        const host = server.address().address;

        console.log(`Static HTTP server is listening at http://${host}:${port}`);
        open(`http://localhost:${port}/`);
    });
});
