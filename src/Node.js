module.exports = class Node {
    /**
     * @param {string} enterText
     * @param {string} enterImage
     * @param {{button:string,target:string}[]} actions actions it will produce
     */
    constructor(enterText, enterImage, actions) {
        this.enterText = enterText;
        this.enterImage = enterImage;
        this.actions = actions;
    }
};
