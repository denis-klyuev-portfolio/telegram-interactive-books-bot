const BaseBook = require('../BaseBook');
const Node = require('../Node');

module.exports = class Book extends BaseBook {
    constructor(id) {
        super(id, 'Муравьи', 'Книга о боевых и не только муравьях');

        this.graph.nodes.start = new Node('Приветствую тебя, мой шестилапый друг!', null, [
            {
                button: 'Уииии!', target: 'afterStart'
            },
            {
                button: 'Понятно', target: 'afterStart'
            }
        ]);
        this.graph.nodes.afterStart = new Node('Итак, тебе надо сделать выбор: кем ты будешь?', null, [
            {
                button: 'Рабочий', target: 'selectedWorker'
            },
            {
                button: 'Вояка', target: 'selectedWarrior'
            }
        ]);
        this.graph.nodes.selectedWorker = new Node('Ты выбрал работать!', null, []);
        this.graph.nodes.selectedWarrior = new Node('Ты выбрал воевать!', null, []);
    }
};
