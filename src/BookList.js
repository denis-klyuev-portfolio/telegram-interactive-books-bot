const fs = require('fs');
const path = require('path');

module.exports = class BookList {
    /**
     * @return {BaseBook}
     */
    static list() {
        if (!this._cache) {
            this._cache = fs.readdirSync(path.resolve(__dirname, './books'))
                .map(file => {
                    console.log('file listing:', file);
                    const c = require(path.resolve(__dirname, `./books/${file}`));
                    const id = parseInt(file.match(/\d+/)[0], 10);
                    return new c(id);
                });
        }

        return this._cache;
    }

    /**
     * @param {number} id
     * @return {BaseBook|undefined}
     */
    static getById(id) {
        return this.list()
            .find(book => book.id === id);
    }

    /**
     * @param {string} title
     * @return {BaseBook|undefined}
     */
    static getByTitle(title) {
        return this.list()
            .find(book => book.title === title);
    }
};
