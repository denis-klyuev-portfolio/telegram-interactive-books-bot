const Message = require('./Message');

test('Simple message parses well', () => {
    const source = {
        "message_id": 7,
        "chat": {
            "id": 371454884,
            "first_name": "Denis",
            "last_name": "Klyuev",
            "username": "denis_klyuev",
            "type": "private"
        },
        "date": 1527575305,
        "text": "/start",
    };
    const parsed = Message.parse(source);
    expect(parsed).toEqual(new Message(7, undefined, 1527575305, source.chat, "/start"));
});
