const User = require('./User');
const Message = require('./Message');

module.exports = class CallbackQuery {
    constructor(id, from, message, inline_message_id, chat_instance, data, game_short_name) {
        this.id = id;
        this.from = User.parse(from);
        this.message = message ? Message.parse(message) : message;
        this.inline_message_id = inline_message_id;
        this.chat_instance = chat_instance;
        this.data = data;
        this.game_short_name = game_short_name;
    }

    /**
     *
     * @param {object} update
     */
    static parse(update) {
        const {
            id,
            from,
            message,
            inline_message_id,
            chat_instance,
            data,
            game_short_name,
        } = update;
        return new CallbackQuery(
            id,
            from,
            message,
            inline_message_id,
            chat_instance,
            data,
            game_short_name,
        );
    }
};
