const User = require('./User');
const Chat = require('./Chat');
const MessageEntity = require('./MessageEntity');

module.exports = class Message {
    /**
     *
     * @param {number} id
     * @param {User} from
     * @param {number} date
     * @param {Chat} chat
     * @param {string} text
     * @param {object[]} entities
     */
    constructor(id, from = null, date, chat, text, entities) {
        this.id = id;
        this.from = from ? User.parse(from) : from;
        this.date = date;
        this.chat = Chat.parse(chat);
        this.text = text;
        this.entities = entities ? entities.map(entity => MessageEntity.parse(entity)) : [];
    }
    /**
     *
     * @param {object} body
     */
    static parse(body) {
        const {message_id: id, from, date, chat, text, entities} = body;
        return new Message(id, from, date, chat, text, entities);
    }
};
