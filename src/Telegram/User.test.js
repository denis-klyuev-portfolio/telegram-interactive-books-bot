const User = require('./User');

test('Simple user parses well', () => {
    const source = {
        "id": 371454884,
        "is_bot": false,
        "first_name": "Denis",
        "last_name": "Klyuev",
        "username": "denis_klyuev",
        "language_code": "ru"
    };
    const parsed = User.parse(source);
    expect(parsed).toEqual(new User(
        source.id,
        source.is_bot,
        source.first_name,
        source.last_name,
        source.username,
        source.language_code,
    ));
});
