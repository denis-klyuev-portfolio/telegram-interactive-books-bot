const Message = require('./Message');
const CallbackQuery = require('./CallbackQuery');

module.exports = class Update {
    /**
     *
     * @param {number} id
     * @param {object} message
     * @param {object} edited_message
     * @param {object} channel_post
     * @param {object} edited_channel_post
     * @param {object} inline_query
     * @param {object} chosen_inline_result
     * @param {object} callback_query
     * @param {object} shipping_query
     * @param {object} pre_checkout_query
     */
    constructor(id,
                message,
                edited_message,
                channel_post,
                edited_channel_post,
                inline_query,
                chosen_inline_result,
                callback_query,
                shipping_query,
                pre_checkout_query) {
        this.id = id;
        this.message = message
            ? Message.parse(message) : message;
        this.edited_message = edited_message
            ? Message.parse(edited_message) : edited_message;
        this.channel_post = channel_post
            ? Message.parse(channel_post) : channel_post;
        this.edited_channel_post = edited_channel_post
            ? Message.parse(edited_channel_post) : edited_channel_post;
        this.inline_query = inline_query;
        this.chosen_inline_result = chosen_inline_result;
        this.callback_query = callback_query
            ? CallbackQuery.parse(callback_query) : callback_query;
        this.shipping_query = shipping_query;
        this.pre_checkout_query = pre_checkout_query;
    }

    /**
     *
     * @param {object} body
     * @return {Update}
     */
    static parse(body) {
        const {
            update_id: id,
            message,
            edited_message,
            channel_post,
            edited_channel_post,
            inline_query,
            chosen_inline_result,
            callback_query,
            shipping_query,
            pre_checkout_query
        } = body;
        return new Update(
            id,
            message,
            edited_message,
            channel_post,
            edited_channel_post,
            inline_query,
            chosen_inline_result,
            callback_query,
            shipping_query,
            pre_checkout_query
        );
    }
};
