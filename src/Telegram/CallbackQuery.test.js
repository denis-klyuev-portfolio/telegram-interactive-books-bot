const CallbackQuery = require('./CallbackQuery');

test('Simple message parses well', () => {
    const source = {
        "id": "1595386578930253452",
        "from": {
            "id": 371454884,
            "is_bot": false,
            "first_name": "Denis",
            "last_name": "Klyuev",
            "username": "denis_klyuev",
            "language_code": "ru"
        },
        "message": {
            "message_id": 29,
            "from": {
                "id": 570783967,
                "is_bot": true,
                "first_name": "Interactive books",
                "username": "InteractiveBooksBot"
            },
            "chat": {
                "id": 371454884,
                "first_name": "Denis",
                "last_name": "Klyuev",
                "username": "denis_klyuev",
                "type": "private"
            },
            "date": 1528065476,
            "text": "hi there"
        },
        "chat_instance": "5992903543217727806",
        "data": "id-1"
    };
    const parsed = CallbackQuery.parse(source);
    expect(parsed).toEqual(new CallbackQuery(
        source.id, source.from, source.message, source.inline_message_id, source.chat_instance,
        source.data, source.game_short_name
    ));
});
