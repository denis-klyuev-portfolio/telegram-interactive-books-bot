module.exports = class User {
    /**
     *
     * @param {number} id
     * @param {boolean} is_bot
     * @param {string} first_name
     * @param {string} last_name
     * @param {string} username
     * @param {string} language_code
     */
    constructor(id, is_bot, first_name, last_name, username, language_code) {
        this.id = id;
        this.is_bot = is_bot;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.language_code = language_code;
    }
    /**
     *
     * @param {string} body
     */
    static parse(body) {
        const {id, is_bot, first_name, last_name, username, language_code} = body;
        return new User(id, is_bot, first_name, last_name, username, language_code);
    }
};
