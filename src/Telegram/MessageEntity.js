const User = require('./User');

module.exports = class MessageEntity {
    /**
     *
     * @param {string} type
     * @param {number} offset
     * @param {number} length
     * @param {string} url
     * @param {User} user
     */
    constructor(type, offset, length, url, user) {
        this.type = type;
        this.offset = offset;
        this.length = length;
        this.url = url;
        this.user = user ? User.parse(user) : user;
    }
    /**
     *
     * @param {object} body
     */
    static parse(body) {
        const {type, offset, length, url, user} = body;
        return new MessageEntity(type, offset, length, url, user);
    }
};
