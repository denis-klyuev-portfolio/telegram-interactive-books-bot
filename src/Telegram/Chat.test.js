const Chat = require('./Chat');

test('Chat parses well', () => {
    const source = {
        "id": 371454884,
        "first_name": "Denis",
        "last_name": "Klyuev",
        "username": "denis_klyuev",
        "type": "private"
    };
    const parsed = Chat.parse(source);
    expect(parsed).toEqual(new Chat(371454884, 'private', undefined, 'denis_klyuev'));
});
