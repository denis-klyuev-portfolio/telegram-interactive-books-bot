const Update = require('./Update');

test('Simple start command parses well', () => {
    const source = {
        "update_id": 53854610,
        "message": {
            "message_id": 7,
            "from": {
                "id": 371454884,
                "is_bot": false,
                "first_name": "Denis",
                "last_name": "Klyuev",
                "username": "denis_klyuev",
                "language_code": "ru"
            },
            "chat": {
                "id": 371454884,
                "first_name": "Denis",
                "last_name": "Klyuev",
                "username": "denis_klyuev",
                "type": "private"
            },
            "date": 1527575305,
            "text": "/start",
            "entities": [
                {
                    "offset": 0,
                    "length": 6,
                    "type": "bot_command"
                }
            ]
        }
    };
    const parsed = Update.parse(source);
    expect(parsed).toEqual(new Update(53854610, source.message));
});
test('Simple button reply parses well', () => {
    const source = {
        "update_id": 53854631,
        "callback_query": {
            "id": "1595386578930253452",
            "from": {
                "id": 371454884,
                "is_bot": false,
                "first_name": "Denis",
                "last_name": "Klyuev",
                "username": "denis_klyuev",
                "language_code": "ru"
            },
            "message": {
                "message_id": 29,
                "from": {
                    "id": 570783967,
                    "is_bot": true,
                    "first_name": "Interactive books",
                    "username": "InteractiveBooksBot"
                },
                "chat": {
                    "id": 371454884,
                    "first_name": "Denis",
                    "last_name": "Klyuev",
                    "username": "denis_klyuev",
                    "type": "private"
                },
                "date": 1528065476,
                "text": "hi there"
            },
            "chat_instance": "5992903543217727806",
            "data": "id-1"
        }
    };
    const parsed = Update.parse(source);
    expect(parsed).toEqual(new Update(53854631, undefined, undefined, undefined, undefined, undefined, undefined, source.callback_query));
});
