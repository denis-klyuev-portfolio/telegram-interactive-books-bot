module.exports = class Chat {
    /**
     *
     * @param {number} id
     * @param {string} type
     * @param {string} title
     * @param {string} username
     */
    constructor(id, type, title, username) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.username = username;
    }
    /**
     *
     * @param {object} body
     */
    static parse(body) {
        const {id, type, title, username} = body;
        return new Chat(id, type, title, username);
    }
};
