module.exports = class Graph {
    /**
     * @param {object} nodes
     * @param {string} start
     */
    constructor(nodes = {}, start = 'start') {
        this.nodes = {...nodes};
        this.start = start;
    }
};
