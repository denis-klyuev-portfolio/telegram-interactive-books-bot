const Item = require('./Item');

module.exports = class MainMenuItem extends Item {
    /**
     * @return {string}
     */
    getMessage() {
        return 'Добро пожаловать в мир интерактивных книг!';
    }
};
