module.exports = class Item {
    constructor(name) {
        this.name = name;
    }

    /**
     * @return {string}
     */
    getMessage() {
        return '**template**';
    }

    /**
     * @return {string[]}
     */
    getButtons() {
        return [];
    }

    canHandleDynamic() {
        return false;
    }
};
