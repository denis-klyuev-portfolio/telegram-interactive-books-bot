const Item = require('./Item');

module.exports = class HelpItem extends Item {
    /**
     * @return {string}
     */
    getMessage() {
        return '**Помощь** тут как тут!';
    }
};
