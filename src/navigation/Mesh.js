// const Item = require('./Item');
const locale = require('../locales/ru');
const MainMenuItem = require('./MainMenuItem');
const HelpItem = require('./HelpItem');
const BookListItem = require('./BookListItem');
const BookItem = require('./BookItem');

module.exports = class Mesh {
    constructor() {
        this.items = {
            mainMenu: new MainMenuItem(locale.BUTTON_MAIN_MENU),
            help: new HelpItem(locale.BUTTON_HELP),
            bookList: new BookListItem(locale.BUTTON_BOOKS_LIST),
            book: new BookItem(),
        };
        const transition = (from, to, showBackButton = false, transitionEntriesList) => ({from, to, showBackButton, transitionEntriesList});
        this.transitions = this._compileTransitions(
            transition('mainMenu', 'bookList'),
            transition('mainMenu', 'help'),

            transition('bookList', 'mainMenu'),
            transition('bookList', 'help'),
            transition('bookList', 'book', false, this.items.book.getTransitionEntriesList()),

            transition('help', 'mainMenu'),
            transition('help', 'bookList'),

            // transition('bookList', 'book'),
            transition('book', 'mainMenu'),
            transition('book', 'help'),
            transition('book', 'book'),
        );

        // console.info('-  TRANSITIONS -');
        // console.info(this.transitions);
        // console.info('- /TRANSITIONS -');
    }

    _compileTransitions(...transitions) {
        const result = {};

        for (const {from, to, showBackButton, transitionEntriesList} of transitions) {
            if (result[from] === undefined) {
                result[from] = {};
            }

            result[from][to] = {showBackButton, transitionEntriesList};
            if (this.items[to]) {
                result[from][to].item = this.items[to];
            }
        }

        return result;
    }

    /**
     * @return {[]}
     */
    getItemsToGo(itemKey) {
        return Object.entries(this.transitions[itemKey] || {});
    }
};
