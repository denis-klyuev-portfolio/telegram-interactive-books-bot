const Item = require('./Item');
const BookList = require('../BookList');

module.exports = class BookListItem extends Item {
    /**
     * @return {string}
     */
    getMessage() {
        return 'У нас есть вот такие замечательные книги';
    }

    /**
     * @return {string[]}
     */
    getButtons() {
        return BookList.list()
            .map(book => book.title);
    }
};
