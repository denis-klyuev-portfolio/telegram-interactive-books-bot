const Item = require('./Item');
const BookList = require('../BookList');
const GameStore = require('../gameStore');

module.exports = class BookItem extends Item {
    canHandleDynamic() {
        return true;
    }

    /**
     * @param {string} input
     * @return {string}
     */
    getMessage(input) {
        const book = BookList.getById(GameStore.user.bookId);
        if (!book) {
            throw Error('Not found');
        }
        console.log('found book', book);
        console.log('graph', book.graph);
        const userBookData = GameStore.user.books[GameStore.user.bookId];
        if (!userBookData.currentNode) {
            userBookData.currentNode = 'start';
            console.log('start fresh');
        } else {
            const currentNode = book.graph.nodes[userBookData.currentNode];
            const nextNode = currentNode.actions.find(({button}) => button === input);
            if (!nextNode) {
                throw Error('Not found');
            }
            userBookData.currentNode = nextNode.target;
        }
        return book.graph.nodes[userBookData.currentNode].enterText;
    }

    /**
     * @return {string[]}
     */
    getButtons() {
        const book = BookList.getById(GameStore.user.bookId);
        if (!book) {
            throw Error('Not found');
        }
        const userBookData = GameStore.user.books[GameStore.user.bookId];
        return book.graph.nodes[userBookData.currentNode].actions.map(({button}) => button);

    }

    /**
     * @return {{id:number,title:string}[]}
     */
    getTransitionEntriesList() {
        return BookList.list()
            .map(book => ({id: book.id, title: book.title}));
    }
};
