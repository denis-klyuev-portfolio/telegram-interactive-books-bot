const Graph = require('./Graph');

module.exports = class BaseBook {
    /**
     * @param {string} title
     * @param {string} description
     */
    constructor(id, title, description) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.graph = new Graph();
    }
};
