const locale = require('./locales/ru');
const User = require('./Telegram/User');
const Mesh = require('./navigation/Mesh');

const GameStore = require('./gameStore');

module.exports = class App {
    /**
     * @param {Update} update
     */
    static async reflectTo(update) {
        // console.log('got an update', update);
        if (update) {
            if (update.message) {
                // TODO: check this approach
                if (update.message.from instanceof User) {
                    await GameStore.loadById(update.message.from.id);
                    // console.log('user', GameStore.user);
                }

                try {
                    return await this.handleMessage(update);
                }
                catch (e) {
                    console.error(e.message);
                    console.error(e.stack);
                    return null;
                }
            }
        }

        return null;
    }

    /**
     * text message received (including bot command)
     * @param {Update} update
     * @return {object}
     */
    static async handleMessage(update) {
        const {message} = update;
        const botCommand = message.entities.find(entity => entity.type === 'bot_command');
        const chatId = message.chat.id;

        let chosenMessage;
        let responseCommand = this.unknownCommand(chatId);

        if (botCommand) {
            const command = message.text.substr(botCommand.offset, botCommand.length);
            switch (command) {
                case '/start':
                    chosenMessage = '/start';
                    break;
                case '/help':
                    chosenMessage = locale.BUTTON_HELP;
                    break;
            }
        } else {
            chosenMessage = message.text;
        }

        const mesh = new Mesh();
        const navigatableItems = mesh.getItemsToGo(GameStore.user.navigationPlace);
        // console.log('navigatableItems before', navigatableItems);

        switch (chosenMessage) {
            case '/start':
            case locale.BUTTON_MAIN_MENU:
                if (GameStore.user.navigationPlace !== null && chosenMessage === '/start') {
                    responseCommand = this.unknownCommand(chatId);
                } else {
                    GameStore.user.navigationPlace = 'mainMenu';
                    responseCommand = this.baseMessage(chatId, 'Добро пожаловать в мир интерактивных книг!');
                }
                break;

            default:
                const targetItem = navigatableItems
                    .find(([key, {item, transitionEntriesList}]) =>
                        (GameStore.user.navigationPlace === key && item && item.canHandleDynamic()) ||
                        (item && item.name === chosenMessage) || (
                        transitionEntriesList && transitionEntriesList.find(({title}) => title === chosenMessage)
                    ));
                if (targetItem) {
                    const [key, {item, transitionEntriesList}] = targetItem;
                    GameStore.user.navigationPlace = key;
                    const enteredByTransition = transitionEntriesList &&
                        transitionEntriesList.find(({title}) => title === chosenMessage);
                    if (enteredByTransition) {
                        GameStore.user.bookId = enteredByTransition.id;
                    }
                    if (GameStore.user.bookId && !GameStore.user.books[GameStore.user.bookId]) {
                        GameStore.user.books[GameStore.user.bookId] = {
                            currentNode: null
                        };
                    }

                    try {
                        const generatedMessage = item.getMessage(chosenMessage);
                        const generatedButtons = item.getButtons();
                        responseCommand = this.baseMessage(chatId, generatedMessage, generatedButtons);
                    }
                    catch (err) {}
                }
        }

        const navigatableItemsAfter = mesh.getItemsToGo(GameStore.user.navigationPlace);
        // console.log('navigatableItems after', navigatableItemsAfter);

        await GameStore.save();

        responseCommand.reply_markup.keyboard = [
            ...responseCommand.reply_markup.keyboard,
            ...navigatableItemsAfter
            .map(([key, {item}]) => [
                ...(item && item.name ? [{text: item.name}] : []),
            ])
        ];

        console.log(responseCommand.reply_markup.keyboard);

        return responseCommand;
    }

    static unknownCommand(chat_id) {
        return this.baseMessage(chat_id, 'Моя твоя не понимайт 🙈🙉🙊');
    }

    static baseMessage(chat_id, text, buttons = []) {
        return {
            method: 'sendMessage',
            text,
            chat_id,
            parse_mode: 'Markdown',
            disable_web_page_preview: true,
            disable_notification: true,
            reply_markup: {
                keyboard: buttons.map(text => [{text}]),
                resize_keyboard: true,
                one_time_keyboard: true,
            }
        };
    }
};
