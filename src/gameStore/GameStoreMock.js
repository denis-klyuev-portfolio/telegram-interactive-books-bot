const fs = require('fs');
const path = require('path');
const defaultState = require('../defaultUserState');
const users = {};

module.exports = class GameStore {
    static async loadById(id) {
        const userId = `${id}`;
        try {
            this.user = await this.get(userId);
            if (!this.user) {
                throw 'Not found';
            }
        }
        catch (e) {
            console.log('get error', e.message);
            try {
                const item = {
                    ...defaultState,
                    userId,
                };
                await this.put(item);
                this.user = item;
            }
            catch (e) {
                console.log('put error', ...args);
            }
        }

        return this.user;
    }

    static async get(userId) {
        return new Promise(resolve => {
            users[userId] = require(path.resolve(__dirname, `../../${userId}.mock.json`));
            resolve(users[userId]);
        });
    }

    static async put(data) {
        users[data.userId] = data;
        return new Promise((resolve, reject) => {
            fs.writeFile(path.resolve(__dirname, `../../${data.userId}.mock.json`), JSON.stringify(data), 'utf8', (err) => {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            })
        });
    }

    static async save() {
        return await this.put(this.user);
    }
};
