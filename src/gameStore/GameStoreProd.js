const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const defaultState = require('../defaultUserState');

module.exports = class GameStore {
    static get tableName() {
        return 'Users';
    }

    static async loadById(id) {
        const userId = `${id}`;
        try {
            this.user = await this.get(userId);
            if (!this.user) {
                throw 'Not found';
            }
        }
        catch (e) {
            console.log('get error', e.message);
            try {
                const item = {
                    ...defaultState,
                    userId,
                };
                await this.put(item);
                this.user = item;
            }
            catch (e) {
                console.log('put error', ...args);
            }
        }

        return this.user;
    }

    static async get(userId) {
        const params = {
            TableName: 'Users',
            Key: {
                userId
            }
        };
        return dynamoDb.get(params).promise().then(response => response.Item);
    }

    static async put(data) {
        const putParams = {
            TableName: this.tableName,
            Item: data
        };

        return dynamoDb.put(putParams).promise();
    }

    static async save() {
        if (!this.user) {
            return;
        }

        const params = {
            TableName: this.tableName,
            Item: this.user
        };

        return await dynamoDb.put(params)
            .promise();
    }
};
