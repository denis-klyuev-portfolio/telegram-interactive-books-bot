const DEBUG = process.env.DEBUG === 'true';

module.exports = DEBUG ? require('./GameStoreMock') : require('./GameStoreProd');
