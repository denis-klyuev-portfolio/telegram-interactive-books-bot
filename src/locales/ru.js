module.exports = {
    BUTTON_HELP: '🔧 Помощь',
    BUTTON_MAIN_MENU: '🔢 Главное меню',
    BUTTON_BOOKS_LIST: 'Список книг',
    BUTTON_BACK: '👈 Назад',
};
