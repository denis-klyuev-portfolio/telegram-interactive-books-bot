module.exports = {
    userId: null,
    books: {},
    bookId: null,
    navigationHistory: [],
    navigationPlace: null,
};
